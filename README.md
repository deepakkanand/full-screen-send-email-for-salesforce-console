# Full Screen Send Email for Salesforce Console
###### Write and send emails from the Salesforce Console using the native Send Email editor in full screen mode.

## Introduction
-----------------------
Most of us, at one point in time will have certainly felt that the Salesforce Console view is too restrictive in terms of the user interface. Say for example you are using the native Send Email and since you are within the console, the pages load within the same Chrome Tab as *sub tabs* and at times the space wouldn't suffice and you would have to end up scrolling up and down while composing the email. This could be a pain. Thus the Chrome Extension allows you to maximize the native Send Email page as well as the Case Detail page to full screen via a **Go Full Screen** button.

## Under the Hood
-----------------------
The Full Screen Send Email for Salesforce Console has been built as a [**Chrome Extension**](https://developer.chrome.com/extensions). 

### How Does it Work
This Chrome Extension uses the concept of [**Content Scripts**](https://developer.chrome.com/extensions/content_scripts) to get it's work done. In simple words, this is a piece of JavaScript code that will be executed on a given web page identified by it's URL pattern. Thus, after you install the Chrome Extension and when you refresh your Console(*say a previously opened Send Email page as a sub tab*), you would notice that there is a new button called "Go Full Screen" like as shown below - 

![Go Full Screen on Send Email](https://bitbucket.org/repo/9p8EGx7/images/470434586-Screenshot_2.png)

That being said, a button will also be made available for the native **Case Detail Pages** too like as shown below - 

![Go Full Screen on Case Detail Page](https://bitbucket.org/repo/9p8EGx7/images/3461465120-Screenshot_3.png)

Now, when the User clicks on one of these buttons, the given frame will be transitioned to Full Screen mode. This is made possible using the [**Full Screen API**](https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API).

### Components
The Chrome Extension primarily contains a *Content Script*. The Content Script houses the JavaScript that is responsible for creating the above mentioned *Go Full Screen* buttons. The target web page(*that page on which the Content Script will be executed upon page load*) is determined in [**manifest.json**](https://bitbucket.org/deepakkanand/full-screen-send-email-for-salesforce-console/src/e5fe0bcf53501ea30c1fd9e946d75ed34cff52cd/manifest.json?at=master&fileviewer=file-view-default).

## Technologies Used
-----------------------
1. Content Scripts
2. Full Screen API

## Queries/Issues/Support
-----------------------
In case of any queries, please reach out to [Deepak](mailto:deepak@dazeworks.com). 

Also the notion of queries does not boil down to just issues/questions. In case you want to learn how to build Chrome Extensions, don't wait, it's just a ping away!