/**
 * We need to run this at every
 * 100 milliseconds since we do
 * not know before hand when the
 * User would open the native "Send-
 * Email" page as a Primary Tab
 * or Sub Tab within the Console.
 */
window.setInterval(
    function() {
        window.top.sfSendEmailpath      = "/_ui/core/email/author/EmailAuthor";
        window.top.sendEmailFrames      = {};

        /**
         * In Salesforce Console, everything is
         * an IFRAME. So collect all the IFRAMEs
         * which has presently loaded the native
         * "Send Email" page in Salesforce and build
         * a hash.
         */
        Array.from( document.getElementsByTagName( "iframe" ) ).forEach(
            function( frame ) {
                if( frame.src.indexOf( sfSendEmailpath ) >= 0 ) {
                    window.top.sendEmailFrames[frame.id] = frame;
                }
            }
        );

        /**
         * If there is atleast one IFRAME
         * that has the "Send Email" page
         * of Salesforce opened...
         */
        if( Object.keys( window.top.sendEmailFrames ).length > 0 ) {
            Object.keys( window.top.sendEmailFrames ).forEach(
                function( frameId ) {
                    /**
                     * Enable the Chrome Fullscreen API
                     * for the IFRAME. We're lucky that
                     * that Console pages are loaded as IFRAME
                     * due to which can use the Chrome Fullscreen
                     * API.
                     */
                    window.top.sendEmailFrames[frameId].allowfullscreen = true;

                    var topBtnRow 		= window.top.sendEmailFrames[frameId].contentDocument.getElementById( "topButtonRow" );
                	var bottomBtnRow 	= window.top.sendEmailFrames[frameId].contentDocument.getElementById( "bottomButtonRow" );
                    
                    /**
                     * Create a button to the right of
                     * the "Cancel" button on the "Send-
                     * Email" page called "Go Full Screen"
                     * which when clicked would send you to
                     * a Full Screen mode.
                     */
                    if( topBtnRow && topBtnRow.innerHTML.indexOf( "Go Full Screen" ) === -1 ) {
                        topBtnRow.innerHTML += 
                            '<input type="button" class="btn" value="Go Full Screen" onclick="window.top.document.getElementById(\'' + frameId + '\').webkitRequestFullscreen()" />';
                    }

                    if( bottomBtnRow && bottomBtnRow.innerHTML.indexOf( "Go Full Screen" ) === -1 ) {
                        bottomBtnRow.innerHTML += 
                            '<input type="button" class="btn" value="Go Full Screen" onclick="window.top.document.getElementById(\'' + frameId + '\').webkitRequestFullscreen()" />';
                    }
                }
            );
        }
    },
    100
);

window.setInterval(
    function() {
        window.top.sfCaseDetailPath = "/500";
        window.top.sfCaseListPath   = "/500?fcf=";
        window.top.skiplinkSelector = "a[name=skiplink]";
        window.top.caseDetailFrames = {};

        /**
         * In Salesforce Console, everything is
         * an IFRAME. So collect all the IFRAMEs
         * which has presently loaded the native
         * "Case Detail" page in Salesforce and not
         * the the "Case List" page and then build
         * a hash.
         */
        Array.from( document.querySelectorAll( "[src*='" + sfCaseDetailPath + "']" ) ).forEach(
            function( frame ) {
                if( 
                    frame.src.indexOf( sfCaseDetailPath )   >= 0    &&
                    frame.src.indexOf( sfCaseListPath )     === -1
                ) {
                    window.top.caseDetailFrames[frame.id] = frame;
                }
            }
        );


        /**
         * If there is atleast one IFRAME
         * that has the "Case Detail" page
         * of Salesforce opened...
         */
        if( Object.keys( window.top.caseDetailFrames ).length > 0 ) {
            Object.keys( window.top.caseDetailFrames ).forEach(
                function( frameId ) {
                    /**
                     * Enable the Chrome Fullscreen API
                     * for the IFRAME. We're lucky that
                     * that Console pages are loaded as IFRAME
                     * due to which can use the Chrome Fullscreen
                     * API.
                     */
                    window.top.caseDetailFrames[frameId].allowfullscreen = true;

                    /**
                     * Create a button to the top
                     * of the Case Detail page
                     * called "Go Full Screen"
                     * which when clicked would
                     * send you to a Full Screen 
                     * mode.
                     */
                    if(
                        window.top.caseDetailFrames[frameId].contentDocument.querySelector( skiplinkSelector ) &&
                        !window.top.caseDetailFrames[frameId].contentDocument.querySelector( "button[name=gofullscreen]" )
                    ) {
                        window.top.caseDetailFrames[frameId].contentDocument.querySelector( skiplinkSelector ).insertAdjacentHTML(
                            "beforeBegin",
                            '<div style="text-align:center; margin-top: 5px;"><button name="gofullscreen" class="btn" onclick="window.top.document.getElementById(\'' + frameId + '\').webkitRequestFullscreen()" >Go Full Screen</button></div>'
                        );
                    }
                }
            );
        }
    },
    100
);